#!/bin/bash

trap 'exit 1' ERR

# a few sanity checks
if [ "$HOSTNAME" != "monote-alpha" ]
then
	echo "Not running on deployment server!"
	exit 1
fi

if [ "$(whoami)" != "monote" ]
then
	echo "Not running as app user!"
	exit 1
fi

echo "Fetching dependencies."
npm install

echo "Migrating database."
npx knex migrate:latest

echo "Building."
npm run build

echo "Deployment complete. Run systemctl restart monote to start the updated server!"
