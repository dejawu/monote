const {
  mysql_host,
  migration_user,
  migration_pass,
  mysql_db,
} = require("./secrets");

module.exports = {
  development: {
    client: "mysql2",
    connection: {
      host: mysql_host,
      user: migration_user,
      password: migration_pass,
      database: mysql_db,
      multipleStatements: true,
    },
  },
};
