exports.up = function(knex) {
  return knex.schema.table("users", table => {
    table.dropColumn("claimed");
    table.dropColumn("expires");
  });
};

exports.down = function(knex) {
  return knex.schema.table("users", table => {
    table.boolean("claimed").notNullable();
    table.integer("expires");
  });
};
