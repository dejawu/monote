exports.up = function(knex) {
  console.log(
    "Please make sure the STRICT_TRANS_TABLES setting in sql_mode is disabled.",
  );
  return knex.raw(`
ALTER TABLE users
  CHANGE \`id\` \`userid\` INT( 10 )
  NOT NULL AUTO_INCREMENT;
`);
};

exports.down = function(knex) {
  console.log(
    "Please make sure the STRICT_TRANS_TABLES setting in sql_mode is disabled.",
  );
  return knex.raw(`
ALTER TABLE users
  CHANGE \`userid\` \`id\` INT( 10 )
  NOT NULL AUTO_INCREMENT;
`);
};
