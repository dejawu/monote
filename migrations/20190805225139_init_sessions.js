// schema SQL from here: https://github.com/chill117/express-mysql-session/blob/master/schema.sql

exports.up = function(knex) {
  return knex.raw(`
CREATE TABLE IF NOT EXISTS \`sessions\` (
  \`session_id\` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  \`expires\` int(11) unsigned NOT NULL,
  \`data\` mediumtext COLLATE utf8mb4_bin,
  PRIMARY KEY (\`session_id\`)
) ENGINE=InnoDB
`);
};

exports.down = function(knex) {
  return knex.schema.dropTable("sessions");
};
