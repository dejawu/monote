exports.up = function(knex) {
  console.log("Please ensure the global event_scheduler is set to ON");
  return knex.raw(`
CREATE EVENT temp_user_cleanup
  ON SCHEDULE EVERY 30 SECOND
  ON COMPLETION PRESERVE
  DO
    DELETE FROM monote.users WHERE UNIX_TIMESTAMP() > expires AND claimed=FALSE;
  `);
};

exports.down = function(knex) {
  return knex.raw(`DROP EVENT IF EXISTS temp_user_cleanup`);
};
