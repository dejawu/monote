exports.up = function(knex) {
  return knex.schema.createTable("users", table => {
    table.increments("id").notNullable();
    table.string("username", 100).notNullable();
    table.string("passhash", 64).notNullable();
    table.unique("username");
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable("users");
};
