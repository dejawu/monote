import React, { Component, useEffect } from "react";

import {
  FiCheckCircle,
  FiAlertCircle,
  FiSave,
  FiEdit3,
  FiCloudOff,
  FiDownloadCloud,
  FiLogIn,
  FiLogOut,
  FiSettings,
  FiUser,
  FiUserPlus,
  FiPenTool,
} from "react-icons/fi";

import { post } from "./lib/fetch";

const Sidebar = ({
  authed,
  username,
  status,
  toc,
  message,
  visible,
  cursorGlobalIndex,
  onLoginSuccess,
  onToCNavigate,
}) => {
  let bodyComponent;
  switch (authed) {
    case undefined:
      bodyComponent = <p>Loading...</p>;
      break;
    case false:
      bodyComponent = <LoggedOutBody onLoginSuccess={onLoginSuccess} />;
      break;
    case true:
      bodyComponent = <LoggedInBody username={username} />;
      break;
  }

  return (
    <div id="sidebar" style={{ width: visible ? 250 : 0 }}>
      <Header status={status} message={message} />
      {bodyComponent}
      <TableOfContents
        status={status}
        toc={toc}
        handleNavigate={onToCNavigate}
        cursorGlobalIndex={cursorGlobalIndex}
      />
    </div>
  );
};

const HEADING_INDENT = 10; // number of pixels to indent for each heading level below 1

// clears all data belonging to authed user
const clearAuthedData = () => {
  Object.keys(localStorage)
    .filter(key => key.startsWith("a_"))
    .forEach(key => delete localStorage[key]);
};

const TableOfContents = ({
  status,
  toc,
  handleNavigate,
  cursorGlobalIndex,
}) => {
  const { id: currentHeadingId } =
    toc
      .slice()
      .reverse()
      .find(
        ({ globalIndex: tocGlobalIndex } = {}) =>
          cursorGlobalIndex >= tocGlobalIndex,
      ) || {};

  useEffect(() => {
    const el = document.getElementById("toc-entry-current");
    if (el) {
      el.scrollIntoView({
        block: "center",
        behavior: "smooth",
      });
    }
  }, [cursorGlobalIndex]);

  return (
    <div id="toc">
      <h3 className="sidebar-header">
        <span>Table of contents</span>
      </h3>
      <ul id="toc-entries">
        {toc.map(({ content, level, id, globalIndex } = {}) => (
          <li
            key={id}
            className={`toc-entry toc-level-${level}`}
            id={currentHeadingId === id ? "toc-entry-current" : undefined}
          >
            <a
              href="#"
              style={{
                paddingLeft: (level - 1) * HEADING_INDENT,
              }}
              onClick={e => {
                e.preventDefault();
                // disable ToC jumping when document is still saving
                if (["write", "save", "load"].includes(status)) {
                  return;
                }
                handleNavigate(globalIndex);
              }}
            >
              {content}
              {currentHeadingId === id && <FiPenTool className="toc-here" />}
            </a>
          </li>
        ))}
      </ul>
    </div>
  );
};

const Header = ({ status, message }) => (
  <>
    <div id="sidebar-logo">
      <h2
        className={["write", "save", "load"].includes(status) ? "rainbow" : ""}
      >
        <span>monote</span>
        {
          {
            init: null,
            write: <FiEdit3 size={20} />,
            save: <FiSave size={20} title="Saving..." />,
            ok: (
              <FiCheckCircle
                size={20}
                className="green"
                title="All changes saved"
              />
            ),
            err: (
              <FiAlertCircle size={20} className="red" title="Failed to save" />
            ),
            dc: <FiCloudOff size={20} className="red" title="Disconnected" />,
            load: (
              <FiDownloadCloud
                size={20}
                className="red"
                title="Loading content"
              />
            ),
          }[status]
        }
      </h2>
    </div>
    <span
      id="sidebar-message"
      className={
        {
          ok: "green",
          err: "red",
          dc: "red",
        }[status]
      }
    >
      {message}
    </span>
  </>
);

// displays when user is logged in
const LoggedInBody = ({ username }) => (
  <>
    <p id="sidebar-user">
      <FiUser /> {username}
    </p>

    <div id="sidebar-buttons">
      <a href="#" className="button-neutral">
        <FiSettings />
        <span>Settings</span>
      </a>
      <a
        href="#"
        className="button-neutral"
        onClick={async e => {
          e.preventDefault();
          const [_, err] = await post("/auth/logout", {});

          clearAuthedData();

          if (err) {
            // TODO show error
            return;
          }

          window.location = "/";
        }}
      >
        <FiLogOut />
        <span>Logout</span>
      </a>
    </div>
  </>
);

// displays when user is not logged in
class LoggedOutBody extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      password: "",
      message: "",
    };
  }

  componentDidMount() {
    // clear any authed user data that may be lingering
    clearAuthedData();
  }

  handleUsernameChange = event => {
    this.setState({ username: event.target.value });
  };

  handlePasswordChange = event => {
    this.setState({ password: event.target.value });
  };

  // TODO show field for user email when user joins
  handleClaimSubmit = async e => {
    e.preventDefault();
    const [res, err] = await post("/auth/claim", {
      username: this.state.username,
      password: this.state.password,
    });

    if (err) {
      console.log(err);
      if (err.body) {
        this.setState({ message: err.body.message });
      }
      return;
    }
    this.props.onLoginSuccess(this.state.username);
  };

  handleLoginSubmit = async e => {
    if (e) {
      e.preventDefault();
    }

    const [res, err] = await post("/auth/login", {
      username: this.state.username,
      password: this.state.password,
    });

    if (err) {
      if (err.body) {
        this.setState({ message: err.body.message });
      }
      return;
    }

    this.props.onLoginSuccess(this.state.username);
  };

  render() {
    return (
      <>
        <input
          type="text"
          onChange={this.handleUsernameChange}
          placeholder="Username"
          onKeyPress={e => {
            if (e.key === "Enter") {
              this.handleLoginSubmit(e);
            }
          }}
        />

        <input
          type="password"
          onChange={this.handlePasswordChange}
          placeholder="Password"
          onKeyPress={e => {
            if (e.key === "Enter") {
              this.handleLoginSubmit(e);
            }
          }}
        />

        <div id="sidebar-buttons">
          <a
            href="#"
            className="button-green"
            onClick={e => this.handleLoginSubmit(e)}
          >
            <FiLogIn />
            <span>Login</span>
          </a>

          <a
            href="#"
            className="button-purple"
            onClick={e => this.handleClaimSubmit(e)}
          >
            <FiUserPlus />
            <span>Get Sync</span>
          </a>
        </div>

        {this.state.message !== "" && <p>{this.state.message}</p>}
      </>
    );
  }
}

export default Sidebar;
