import "regenerator-runtime/runtime";

import React, { Component } from "react";
import { render } from "react-dom";

import io from "socket.io-client";

import { get } from "./lib/fetch";
const { domain: DOMAIN, port: PORT, env: ENV } = require("../../../secrets");
const HOST = `${DOMAIN}:${PORT}`;

import Sidebar from "./Sidebar";
import Editor from "./Editor/Editor";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      status: "init", // one of "init", "write", "save", "ok", "err", "dc", "load", "panic"
      message: "Pre-alpha preview",
      toc: [],
      cursorIndex: -1, // sentinel value for unset
      sidebarWidth: 250,
      sidebarVisible: true,
    };

    // ok to fire and forget
    this.init();
  }

  async init() {
    const [{ authed, username } = {}, err] = await get("/auth/touch");
    if (err) {
      console.log(err);
      return;
    }

    this.setState({
      authed,
      username,
      message: authed ? `Welcome, ${username}.` : null,
    });

    if (authed) {
      const ticketId = await this.getTicket();
      this.initSocket(ticketId);
    }
  }

  async getTicket() {
    try {
      const [{ ticketId } = {}, err] = await get("/auth/ticket");
      if (err) {
        // TODO handle error
        console.log("Error...");
        console.log(err);
      }
      return ticketId;
    } catch (err) {
      return null;
    }
  }

  async initSocket(ticketId) {
    if (!ticketId) {
      ticketId = await this.getTicket();
      console.log("Got ticket: " + ticketId);
    }

    // TODO wss
    this.socket = io("ws://" + HOST, {
      transports: ["websocket", "polling"],
      query: { ticketId },
      forceNew: true,
    });

    this.socket.on("connect", () => {
      console.log("Connected");
      this.handleMessage("Reconnected.", "init");
    });

    this.socket.on("error", error => {
      console.log("error:");
      console.log(error);
    });

    this.socket.on("reconnecting", async attemptN => {
      let ticketId = await this.getTicket();
      if (ticketId) {
        // restart with fresh socket.
        this.socket.close();
        this.initSocket(ticketId);
      }
    });

    this.socket.on("disconnect", reason => {
      this.handleMessage("Disconnected.", "dc");
    });
  }

  handleMessage = (message, status, debug = null) => {
    // pass any debug info
    if (debug) {
      // TODO error reporting
      console.log(debug);
    }

    const newState = {};

    if (message) {
      newState.message = message;
    }

    if (status) {
      newState.status = status;
    }

    this.setState(newState);
  };

  render() {
    return (
      <>
        {ENV === "DEV" && <div id="dev-warning">DEV BUILD</div>}
        <Sidebar
          authed={this.state.authed}
          username={this.state.username}
          status={this.state.status}
          message={this.state.message}
          toc={this.state.toc}
          cursorGlobalIndex={this.state.cursorIndex}
          onLoginSuccess={() => {
            window.location = "/";
          }}
          // passes scroll target node index from sidebar to editor
          onToCNavigate={globalIndex =>
            this.editorRef.scrollToNode(globalIndex)
          }
          width={this.state.sidebarWidth}
          visible={this.state.sidebarVisible}
          // ref={r => (this.sidebarRef = r)}
        />
        {this.state.status === "panic" ? (
          <h1>
            The editor has crashed. Please refresh the page.
            {/* TODO: offer to send an error report */}
          </h1>
        ) : (
          <Editor
            authed={this.state.authed}
            // allows editor to set status message and icon
            onMessage={this.handleMessage}
            // passes full ToC info from editor to sidebar
            onToCChange={toc => this.setState({ toc })}
            // passes cursor global node index from editor to sidebar
            onCursorMove={globalIndex =>
              this.setState({ cursorIndex: globalIndex })
            }
            // handles update to single heading in ToC
            onHeadingUpdate={newHeading => {
              this.setState(({ toc }) => {
                const newToc = toc.slice();
                let i = newToc.findIndex(({ id } = {}) => id === newHeading.id);
                if (i !== -1) {
                  // heading exists; modify or delete
                  if (newHeading.content) {
                    newToc[i].content = newHeading.content;
                  } else {
                    delete newToc[i];
                  }
                } else {
                  // heading does not exist; add it
                  newToc.push(newHeading);
                }

                newToc.sort(({ globalIndex: a }, { globalIndex: b }) => a - b);

                return { toc: newToc };
              });
            }}
            onSidebarResize={newWidth =>
              this.setState({ sidebarWidth: newWidth })
            }
            onSidebarToggle={() =>
              this.setState(({ sidebarVisible }) => ({
                sidebarVisible: !sidebarVisible,
              }))
            }
            ref={r => (this.editorRef = r)}
          />
        )}
      </>
    );
  }
}

render(<App />, document.getElementById("app"));
