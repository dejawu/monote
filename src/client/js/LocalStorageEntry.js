import shortid from "shortid";

// get string id of next node in localstorage, including namespace
const getNextId = storageItem =>
  storageItem.substring(0, storageItem.indexOf("|"));
// get serialized JSON as text
const getNodeStr = storageItem =>
  storageItem.substring(storageItem.indexOf("|") + 1, storageItem.length);

const localStorageEntry = namespace =>
  class LocalStorageEntry {
    constructor(id = null) {
      if (!id) {
        id = shortid.generate();
      }

      if (id.startsWith(namespace + "_")) {
        this.id = id;
      } else {
        this.id = namespace + "_" + id;
      }

      let storageItem = localStorage.getItem(this.id);

      if (!storageItem) {
        storageItem = `${namespace}_tail*|null`;
        localStorage.setItem(this.id, storageItem);
      }

      this.nextId = getNextId(storageItem);
      // don't parse JSON until necessary
      this.nodeStr = getNodeStr(storageItem);
    }

    static get head() {
      return new LocalStorageEntry("head*");
    }

    // head cannot be set; simply getting it creates the entry
    static set head(entry) {
      throw new Error();
    }

    static get version() {
      return localStorage.getItem(namespace + "_version");
    }

    static set version(version) {
      localStorage.setItem(namespace + "_version", version);
    }

    _write() {
      localStorage.setItem(this.id, `${this.nextId}|${this.nodeStr}`);
    }

    get node() {
      return JSON.parse(this.nodeStr);
    }

    set node(node) {
      if (typeof node === "object") {
        node = JSON.stringify(node);
      }

      this.nodeStr = node;
      this._write();
    }

    get next() {
      return this.nextId === `${namespace}_tail*`
        ? null
        : new LocalStorageEntry(this.nextId);
    }

    set next(entry) {
      this.nextId = entry.id;
      this._write();
    }

    hasNext() {
      return !(this.nextId === `${namespace}_tail*` || this.nodeStr === "null");
    }

    delete() {
      localStorage.removeItem(this.id);
    }
  };

export default localStorageEntry;
