import {
  baseKeymap as originalBaseKeymap,
  chainCommands,
  toggleMark,
  wrapIn,
  setBlockType,
} from "prosemirror-commands";
import {
  splitListItem,
  sinkListItem,
  liftListItem,
} from "prosemirror-schema-list";
import { undo, redo } from "prosemirror-history";

import schema from "../../../common/schema";

import shortid from "shortid";

const changeHeadingSize = direction => (state, dispatch) => {
  const { anchor } = state.selection;
  // also check one step before, to include the cursor sitting at the end of a heading block
  const node =
    state.doc.nodeAt(anchor) || state.doc.nodeAt(Math.max(anchor - 1, 0));

  if (!node) {
    return false;
  }

  let resolvedAnchor = state.doc.resolve(anchor);

  const parentNode = resolvedAnchor.parent;

  let tr;
  if (parentNode.type.name !== "heading") {
    // not already heading; make it one
    tr = state.tr.setBlockType(anchor, undefined, schema.nodes.heading, {
      level: { up: 1, down: 6 }[direction],
      id: shortid.generate(),
    });
  } else {
    // set heading level
    tr = state.tr.setBlockType(anchor, undefined, schema.nodes.heading, {
      level: {
        up: Math.max(parentNode.attrs.level - 1, 1),
        down: Math.min(parentNode.attrs.level + 1, 6),
      }[direction],
      id: parentNode.attrs.id,
    });
  }

  if (dispatch) {
    dispatch(tr.scrollIntoView());
  }

  return (
    parentNode.type.name !== "heading" ||
    {
      up: parentNode.attrs.level > 1,
      down: parentNode.attrs.level < 6,
    }[direction]
  );
};

// preset commands for common editing actions
const menuCommands = {
  undo,
  redo,
  heading_up: changeHeadingSize("up"),
  heading_down: changeHeadingSize("down"),
  paragraph: setBlockType(schema.nodes.paragraph),
  bold: toggleMark(schema.marks.strong),
  italic: toggleMark(schema.marks.em),
  blockquote: wrapIn(schema.nodes.blockquote),
  code: toggleMark(schema.marks.code),
  code_block: setBlockType(schema.nodes.code_block),
};

const baseKeymap = {
  ...originalBaseKeymap,
  Enter: chainCommands(
    splitListItem(schema.nodes.list_item),
    originalBaseKeymap.Enter,
  ),
  // last function always returns true to stop normal tab behavior from occurring
  Tab: chainCommands(sinkListItem(schema.nodes.list_item), () => true),
  "Shift-Tab": chainCommands(liftListItem(schema.nodes.list_item), () => true),
  "Mod-z": undo,
  "Mod-shift-z": redo,
  "Mod-y": redo,
  "Mod-s": () => true,
  "Mod-a": () => true,
  "Mod-b": menuCommands.bold,
  "Mod-i": menuCommands.italic,
};

export { baseKeymap, chainCommands, menuCommands, splitListItem, toggleMark };
