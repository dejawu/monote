import React from "react";
import { menuCommands } from "./commands";
import { FiSidebar } from "react-icons/fi";

const commandBodies = {
  undo: "↶",
  redo: "↷",
  heading_up: (
    <strong style={{ fontSize: "18px", lineHeight: "22px" }}>
      H<sup>+</sup>
    </strong>
  ),
  heading_down: (
    <strong style={{ fontSize: "18px", lineHeight: "22px" }}>
      H<sup>-</sup>
    </strong>
  ),
  paragraph: "¶",
  bold: <strong>B</strong>,
  italic: <em>I</em>,
  code: <pre style={{ fontSize: 10 }}>&lt;/&gt;</pre>,
  code_block: (
    <pre
      style={{
        lineHeight: 1.0,
        textAlign: "center",
        fontSize: 10,
        margin: "4px 0",
      }}
    >
      &lt;&gt;
      <br />
      &lt;/&gt;
    </pre>
  ),
  blockquote: (
    <blockquote
      style={{
        paddingLeft: 3,
        height: "75%",
      }}
    >
      ❞
    </blockquote>
  ),
};

const commandTitles = {
  undo: "Undo",
  redo: "Redo",
  heading_up: "Larger heading",
  heading_down: "Smaller heading",
  paragraph: "Plain text",
  bold: "Bold",
  italic: "Italic",
  code: "Inline code",
  code_block: "Code block",
  blockquote: "Block quote",
};

const MenuBar = ({
  editorView,
  commands,
  enabled,
  // onSidebarResize,
  onSidebarToggle,
}) => (
  <div id="menubar">
    <h4
      id="menubar-toc"
      draggable="true"
      // onDragStart={event => {
      //   event.dataTransfer.setDragImage(new Image(), 0, 0);
      // }}
      // onDrag={event => {
      //   onSidebarResize(Math.max(event.clientX, 220));
      // }}
      onClick={event => {
        onSidebarToggle();
      }}
      title="Hide sidebar"
    >
      <FiSidebar style={{ pointerEvents: "none" }} />
    </h4>
    {commands.map(command => (
      <h4
        key={command}
        onClick={e => {
          e.preventDefault();
          if (!enabled[command]) {
            return;
          }

          editorView.focus();
          menuCommands[command](
            editorView.state,
            editorView.dispatch,
            editorView,
          );
        }}
        title={commandTitles[command]}
        className={enabled && enabled[command] ? "" : "disabled"}
      >
        {commandBodies[command]}
      </h4>
    ))}
  </div>
);

export default MenuBar;
