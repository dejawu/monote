import React, { Component } from "react";

import ReadWriteLock from "rwlock";
import shortid from "shortid";

import { Decoration, DecorationSet, EditorView } from "prosemirror-view";
import { EditorState, Plugin, TextSelection } from "prosemirror-state";
import { Slice } from "prosemirror-model";
import { keymap } from "prosemirror-keymap";
import {
  inputRules,
  textblockTypeInputRule,
  wrappingInputRule,
} from "prosemirror-inputrules";
import { history } from "prosemirror-history";

import MenuBar from "./Menubar";
import schema from "../../../common/schema";

import Storage from "../../../common/Storage";
import SyncedStorage from "../SyncedStorage";
import localStorageEntry from "../LocalStorageEntry";

import { baseKeymap, menuCommands } from "./commands";

import markInputRule from "./markInputRule";

import { chunk_size as CHUNK_SIZE } from "../../../../secrets";

// when the end of content is this number of pixels away or fewer from the viewport edge, load until it is not
const SCROLL_LOAD_THRESHOLD = 1000;
// when the end of content is this number of pixels away or more from the viewport edge, unload until it is not
const SCROLL_UNLOAD_THRESHOLD = 2 * SCROLL_LOAD_THRESHOLD; // not a hard-and-fast factor
/*
load and unload thresholds should have enough distance between them that they are not likely to repeatedly load and unload
the same content. the unload threshold must be greater than the load threshold.
*/

let lastContentLoad = 0;

class Editor extends Component {
  constructor(props) {
    super(props);

    this.state = {};

    this.editorRef = null;
    this.pmRef = null;

    const lastGlobalIndex = parseInt(localStorage.getItem("lastGlobalIndex"));
    if (lastGlobalIndex && !Number.isNaN(lastGlobalIndex) && lastGlobalIndex >= 0) {
      this.begin = lastGlobalIndex;
      this.end = lastGlobalIndex;
    } else {
      this.begin = 0; // global index of first node loaded
      this.end = 0; // global index of last node loaded
    }

    this.lock = new ReadWriteLock();

    this.plugins = [
      history(),
      keymap(baseKeymap),
      inputRules({
        // Markdown shortcuts (type Markdown syntax to apply WYSIWYM styles)
        rules: [
          wrappingInputRule(/^\s*>\s$/, schema.nodes.blockquote),
          wrappingInputRule(
            /^(\d+)\.\s$/,
            schema.nodes.ordered_list,
            match => ({ order: +match[1] }),
            (match, node) => node.childCount + node.attrs.order == +match[1],
          ),
          wrappingInputRule(/^\s*([-+*])\s$/, schema.nodes.bullet_list),
          wrappingInputRule(/^\s*```\s$/, schema.nodes.code_block),
          textblockTypeInputRule(
            new RegExp("^(#{1,6})\\s$"),
            schema.nodes.heading,
            match => ({ level: match[1].length, id: shortid.generate() }),
          ),
          markInputRule(/(?:\*\*|__)([^*_]+)(?:\*\*|__)$/, schema.marks.strong),
          markInputRule(/(?:^|[^_])(_([^_]+)_)$/, schema.marks.em),
          markInputRule(/(?:^|[^*])(\*([^*]+)\*)$/, schema.marks.em),
          markInputRule(/(?:^|[^*])(\`([^*]+)\`)$/, schema.marks.code),
          markInputRule(
            /\[(\S+)]\(((?:http(s)?:\/\/.)?[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b(?:[-a-zA-Z0-9@:%_+.~#?&/=]*))\)$/,
            schema.marks.link,
          ),
        ],
      }),
      // plugin for highlight-on-scroll decorations
      new Plugin({
        state: {
          init(_, { doc }) {
            return DecorationSet.create(doc, []);
          },
          apply(tr, decorationSet) {
            const boundary = tr.getMeta("highlightScrolledTo");
            if (!boundary && !tr.getMeta("keepScrollHighlight")) {
              return decorationSet
                .remove(decorationSet.find())
                .map(tr.mapping, tr.doc);
            }

            if (tr.getMeta("keepScrollHighlight")) {
              // no-op, keep current decoration set
              return decorationSet.map(tr.mapping, tr.doc);
            }

            return decorationSet
              .remove(decorationSet.find())
              .add(tr.doc, [
                Decoration.inline(boundary[0], boundary[1], {
                  class: "scrollHighlight",
                }),
              ])
              .map(tr.mapping, tr.doc);
          },
        },
        props: {
          decorations(state) {
            return this.getState(state);
          },
        },
      }),
      // general plugin for everything that should be run after a view update
      new Plugin({
        view: view => {
          return {
            update: () => {
              const { onCursorMove, onHeadingUpdate } = this.props;

              const localIndex = view.state.doc
                .resolve(view.state.selection.$anchor.before(1))
                .index();
              this.lock.writeLock(release => {
                const globalIndex = Math.max(localIndex + this.begin, 0);

                onCursorMove(globalIndex);

                // send heading changes, if applicable
                const node = view.state.doc.maybeChild(localIndex);
                if (node && node.type.name === "heading") {
                  onHeadingUpdate({
                    ...node.attrs,
                    content: node.textContent,
                    globalIndex,
                  });
                }

                clearTimeout(this.saveLastIndexTimeout);
                this.saveLastIndexTimeout = setTimeout(() => {
                  localStorage.setItem("lastGlobalIndex", globalIndex);
                }, 300);

                // update active/disabled state of menu bar buttons
                const commands = Object.keys(menuCommands);
                this.setState({
                  enabledMenuButtons: commands.reduce(
                    (acc, command) => ({
                      ...acc,
                      [command]: menuCommands[command](view.state, null, view),
                    }),
                    {},
                  ),
                });
                release();
              });
            },
          };
        },
      }),
    ];

    this.editorState = EditorState.create({
      schema,
      plugins: this.plugins,
    });

    this.canLoad = true;
    this.lastScrollTop = -1;
  }

  // loads in content as needed
  async loadInfiniteContent() {
    // only allow once load handler to run at a time
    if (!this.canLoad || Date.now() - lastContentLoad < 100) {
      return;
    }

    this.canLoad = false;

    const { onMessage } = this.props;

    // prepends one chunk onto the end of the local document
    // returns true if loadContent should run this again
    const loadBefore = () => {
      // if we are the beginning of the global document we don't need to do anything
      if (this.begin <= 0) {
        return;
      }

      onMessage(null, "load");

      return new Promise((resolve, reject) => {
        this.lock.writeLock(async release => {
          // this.begin is the last node loaded, so we need to start from the one before
          const globalIndex = Math.max(this.begin - 1, 0);

          // TODO wrap in try-catch for storage errors
          const {
            slice: sliceJSON,
            nodeCount,
            begin,
          } = await this.storage.read({
            index: globalIndex - (CHUNK_SIZE - 1),
            count: CHUNK_SIZE,
          });

          if (!sliceJSON || this.begin === 0) {
            release();
            resolve(false);
            return;
          }

          // TODO catch invalid JSON
          const slice = Slice.fromJSON(schema, sliceJSON);
          this.nodeCount = nodeCount;
          const nodesAdded = begin < this.begin;
          if (nodesAdded) {
            // only move "begin" if appending took place, never forwards
            this.begin = begin;
          }

          const tr = this.editorState.tr;
          tr.insert(0, slice.content);
          tr.setMeta("keepScrollHighlight", true);
          tr.setMeta("addToHistory", false);

          this.editorState = this.editorState.apply(tr);
          release();
          resolve(nodesAdded);
        });
      });
    };

    // appends one chunk onto the end of the local document
    // returns true if loadContent should run this again
    const loadAfter = () => {
      // if begin and end are the same, no nodes have been loaded and the editor is empty.
      const init = this.begin === this.end;

      // if we are at the end of the global document, we don't need to do anything
      if (!init && this.end + 1 >= this.nodeCount) {
        return false;
      }

      onMessage(null, "load");

      return new Promise((resolve, reject) => {
        this.lock.writeLock(async release => {
          // this.end is the last node loaded, so we need to start from the next one
          let globalIndex = this.end + 1;
          if (init) {
            globalIndex = this.end;
          }

          // TODO wrap in try-catch for storage errors
          const { slice: sliceJSON, nodeCount, end, toc } =
            (await this.storage.read({
              index: globalIndex,
              count: CHUNK_SIZE,
              toc: init, // only request ToC if first load
            })) || {};

          // if we started off at an invalid index, fix it and start over
          if (init && this.end >= nodeCount) {
            this.begin = Math.max(nodeCount - 1, 0);
            this.end = nodeCount - 1;
            localStorage.setItem("lastGlobalIndex", nodeCount - 1);
            release();
            resolve(true);
            return;
          }

          if (!sliceJSON || (this.end === nodeCount - 1 && !init)) {
            release();
            resolve(false);
            return;
          }

          // TODO catch invalid JSON
          const slice = Slice.fromJSON(schema, sliceJSON);
          this.nodeCount = nodeCount;
          const nodesAdded = end > this.end;
          if (nodesAdded) {
            // only move end if appending took place, never backwards
            this.end = end;
          }

          const tr = this.editorState.tr;

          if (init) {
            tr.replace(0, tr.doc.content.size, slice);
          } else {
            tr.insert(this.editorState.doc.content.size, slice.content);
          }

          tr.setMeta("keepScrollHighlight", true);
          tr.setMeta("addToHistory", false);

          this.editorState = this.editorState.apply(tr);

          if (toc) {
            this.props.onToCChange(toc);
          }

          release();
          resolve(nodesAdded);
        });
      });
    };

    let { scrollHeight, scrollTop, clientHeight } = this.pmRef;

    // while (
    //   scrollHeight - (scrollTop + clientHeight) >
    //     clientHeight + SCROLL_UNLOAD_THRESHOLD &&
    //   (await unloadAfter())
    // ) {
    //   this.dispatchTransaction();
    //   ({ scrollHeight, scrollTop, clientHeight } = this.pmRef);
    // }
    //
    // this.dispatchTransaction();
    //
    // while (scrollTop > SCROLL_UNLOAD_THRESHOLD && (await unloadBefore())) {
    //   const origScrollTop = scrollTop;
    //   const origScrollHeight = scrollHeight;
    //   this.dispatchTransaction();
    //
    //   ({ scrollHeight, scrollTop, clientHeight } = this.pmRef);
    //
    //   this.pmRef.scrollTop = origScrollTop + scrollHeight - origScrollHeight;
    // }
    //
    // this.dispatchTransaction();

    while (
      scrollHeight - (scrollTop + clientHeight) <=
        clientHeight + SCROLL_LOAD_THRESHOLD &&
      (await loadAfter())
    ) {
      this.dispatchTransaction();
      ({ scrollHeight, scrollTop, clientHeight } = this.pmRef);
    }

    // update view with any transactions that may not yet have gone through
    this.dispatchTransaction();

    while (scrollTop <= SCROLL_LOAD_THRESHOLD && (await loadBefore())) {
      const origScrollTop = scrollTop;
      const origScrollHeight = scrollHeight;
      this.dispatchTransaction();

      ({ scrollHeight, scrollTop, clientHeight } = this.pmRef);

      this.pmRef.scrollTop = origScrollTop + scrollHeight - origScrollHeight;
    }

    // update view and scroll height with any transactions that may not yet have gone through
    const origScrollTop = scrollTop;
    const origScrollHeight = scrollHeight;

    this.dispatchTransaction();
    ({ scrollHeight } = this.pmRef);

    this.pmRef.scrollTop = origScrollTop + scrollHeight - origScrollHeight;
    lastContentLoad = Date.now();

    onMessage(null, "ok");

    console.log("Done loading", this.begin, this.end, this.nodeCount);
    this.lastScrollTop = scrollTop;

    this.canLoad = true;
  }

  createEditorView = async element => {
    this.editorRef = element;

    if (this.props.authed) {
      this.storage = new SyncedStorage(localStorageEntry("a"));
    } else {
      this.storage = new Storage(localStorageEntry("l"));
    }

    await this.storage.init();
    this.props.onToCChange(this.storage.toc);

    if (element !== null) {
      this.editorView = new EditorView(element, {
        state: this.editorState,
        scrollMargin: this.editorRef.clientHeight / 2,
        dispatchTransaction: tr => this.dispatchTransaction(tr),
      });

      this.pmRef = this.editorRef.getElementsByClassName("ProseMirror")[0];

      this.loadInfiniteContent();
    }
  };

  // jumps editor to node at global index; called externally via ref
  scrollToNode = async globalIndex => {
    this.editorView.focus();
    const shouldLoad = await new Promise((resolve, reject) => {
      if (globalIndex < this.begin || globalIndex > this.end) {
        // outside of loaded range; need to load
        this.lock.writeLock(async release => {
          this.begin = globalIndex;
          this.end = globalIndex;
          // resetting nodeCount allows loadAfter to make a fresh initial fetch
          this.nodeCount = 0;
          this.editorState = EditorState.create({
            schema,
            plugins: this.plugins,
          });
          release();
          resolve(true);
        });
      } else {
        resolve(false);
      }
    });
    if (shouldLoad) {
      // dispatch reset transaction to view so it resets as well
      this.dispatchTransaction();
      await this.loadInfiniteContent();
    }
    this.highlightNode(globalIndex);
    this.dispatchTransaction();
  };

  // apply highlight decoration to node at given (global) index
  highlightNode(globalIndex) {
    const targetLocalIndex = globalIndex - this.begin;

    let targetPos = -1;
    let targetNodeBegin = -1;
    this.editorState.doc.forEach((node, offset, nodeLocalIndex) => {
      if (targetLocalIndex === nodeLocalIndex) {
        targetPos = offset + node.nodeSize - 1;
        targetNodeBegin = offset;
      }
    });

    if (targetPos === -1) {
      return;
    }

    const resolvedPos = this.editorState.doc.resolve(targetPos);

    const tr = this.editorState.tr;
    tr.setSelection(new TextSelection(resolvedPos));
    tr.setMeta("highlightScrolledTo", [targetNodeBegin, targetPos]);
    tr.scrollIntoView();
    this.editorState = this.editorState.apply(tr);
  }

  // call without transaction parameter to reload view only
  dispatchTransaction = (tr = null) => {
    this.lock.writeLock(release => {
      if (tr) {
        try {
          this.editorState = this.editorState.apply(tr);
        } catch (error) {
          // only creates a problem if losing the transaction means losing steps
          if (tr.steps.length !== 0) {
            this.props.onMessage("Failed to save document.", "err", {
              message: error.message,
            });
            return;
          } else {
            console.log(
              "WARNING: Ignoring nonfatal transaction failure: ",
              error,
            );
            console.log(tr);
          }
        }

        // save document if there were changes to content
        if (tr.steps.length > 0) {
          const { onMessage, onToCChange } = this.props;
          onMessage("Saving...", "write");

          // works correctly here whether saveTimeoutId is set or not
          clearTimeout(this.saveTimeoutId);
          this.saveTimeoutId = setTimeout(() => {
            this.lock.writeLock(async release => {
              // TODO wrap in try-catch for storage errors
              const {
                newEnd,
                nodeCount,
                toc,
                success,
                message,
              } = await this.storage.edit({
                slice: new Slice(this.editorState.doc.content, 0, 0).toJSON(),
                begin: this.begin,
                end: this.end,
                toc: true,
              });

              if (!success) {
                onMessage("Failed to save document.", "err", {
                  message,
                  begin: this.begin,
                  end: this.end,
                });
                return;
              }

              this.end = newEnd;
              this.nodeCount = nodeCount;

              const headingTr = this.editorState.tr;

              // apply heading id's set by storage to local headings
              this.editorState.doc.forEach(
                (
                  {
                    type: { name: typeName } = {},
                    attrs: { level, id } = {},
                    content,
                  } = {},
                  offset,
                  nodeLocalIndex,
                ) => {
                  if (typeName !== "heading") {
                    return;
                  }

                  const nodeGlobalIndex = nodeLocalIndex + this.begin;
                  const tocData =
                    toc.find(
                      ({ globalIndex: tocGlobalIndex }) =>
                        tocGlobalIndex === nodeGlobalIndex,
                    ) || {};
                  if (!id || id !== tocData.id) {
                    headingTr.setNodeMarkup(offset, null, {
                      level,
                      id: tocData.id,
                    });
                  }
                },
              );

              this.editorState = this.editorState.apply(headingTr);
              this.editorView.updateState(this.editorState);

              onToCChange(toc);

              onMessage("All changes saved.", "ok");
              release();
            });
          }, 1000);
        }
      }
      this.editorView.updateState(this.editorState);
      release();
    });
  };

  render() {
    if (typeof this.props.authed === "undefined") {
      return <p>Loading...</p>;
    }

    return (
      <div
        id="editor"
        className="document"
        onScroll={() => this.loadInfiniteContent()}
        ref={this.createEditorView}
      >
        <MenuBar
          commands={Object.keys(menuCommands)}
          editorView={this.editorView}
          enabled={this.state.enabledMenuButtons}
          onSidebarResize={this.props.onSidebarResize}
          onSidebarToggle={this.props.onSidebarToggle}
        />
      </div>
    );
  }
}

export default Editor;
