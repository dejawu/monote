import { chunk_size as CHUNK_SIZE } from "../../../secrets";
import Storage from "../../common/Storage";

import { get, post } from "./lib/fetch";

const apiRead = async ({ index, count = 1, toc = false }) => {
  const [res, error] =
    (await get("/api/v1/read", {
      index,
      count,
      toc,
    })) || [];

  // TODO real error handling
  if (error) {
    return { success: false };
  }

  return res;
};

const apiEdit = async (...params) => {
  const [res, error] =
    (await post("/api/v1/edit", {
      ...params[0],
    })) || [];

  // TODO real error handling
  if (error) {
    return { success: false };
  }

  return res;
};

const apiMeta = async () => {
  const [res, error] = (await get("/api/v1/meta", {})) || [];

  // TODO real error handling
  if (error) {
    return { success: false };
  }

  return res;
};

class SyncedStorage extends Storage {
  constructor(entryClass) {
    super(entryClass);
  }

  // must run to completion before attempting any transactions!
  async init() {
    const { version } = (await apiMeta()) || [];

    // if version is out of date or missing altogether, fetch anew
    if (
      !this.Entry.version ||
      version.toString() !== this.Entry.version.toString()
    ) {
      // fetch fresh version from cloud
      let i = 0;
      let slice, nodeCount, end;
      let entry = this.Entry.head;

      do {
        ({ slice, nodeCount, end } =
          (await apiRead({
            index: i,
            count: CHUNK_SIZE,
            toc: true,
          })) || {});

        if (slice === null) {
          break;
        }

        slice.content.forEach(node => {
          entry.node = node;
          entry.next = new this.Entry();
          entry = entry.next;
        });

        i = end + 1;
      } while (end !== nodeCount);

      this.Entry.version = version;
    }

    super.buildToC();
  }

  // TODO gracefully fail sync
  // slice is a JSON slice, not full PM node
  async edit(params) {
    const { slice, begin, end } = params;
    // perform API call
    const apiResult = await apiEdit({
      ...params,
      version: this.Entry.version,
    });
    // use server-side ToC ids as canon

    const { toc } = apiResult;

    slice.content.forEach((node, i) => {
      if (node.type === "heading") {
        const nodeGlobalIndex = i + begin;
        const { id: tocId } =
          toc.find(el => el.globalIndex === nodeGlobalIndex) || {};
        if (!tocId) {
          throw new Error();
        }
        if (node.attrs.id !== tocId) {
          console.log(
            `Warning: overwriting local id ${node.attrs.id} with server-provided id ${tocId}`,
          );
          node.attrs.id = tocId;
        }
      }
    });

    // call local edit with updated slice
    const localResult = await super.edit({ ...params });

    // TODO check ToC agreement
    if (
      localResult.begin !== apiResult.begin ||
      localResult.end !== apiResult.end
    ) {
      return {
        success: false,
        message: "Client and server results did not agree.",
      };
    }

    // overwrite locally set version number with server-provided version
    this.Entry.version = apiResult.version;

    return {
      ...localResult,
    };
  }
}

export default SyncedStorage;
