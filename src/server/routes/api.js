const call = require("../lib/call");
const { jsonSuccessOut, jsonErrorOut } = require("../lib/jsonOut");
const requireFields = require("../lib/requireFields");

const express = require("express");
const api = express.Router();

module.exports = ({ storage, knex, log }) => {
  // auth-checking middleware
  api.use(async (request, response, next) => {
    if (!request.session) {
      log.warn({
        message: "Request session not set.",
      });
      jsonErrorOut(response, 401);
      next(new Error());
      return;
    }

    const missingField = requireFields(["username", "userid"], request.session);

    if (missingField) {
      log.warn({
        message: "Session was missing field '" + missingField + "', aborting.",
        session: request.session,
      });
      jsonErrorOut(response, 401);
      next(new Error());
      return;
    }

    // check that user is valid
    let [result, err] = await call(
      knex("users")
        .select("userid")
        .where({
          userid: request.session.userid,
          username: request.session.username,
        }),
    );

    if (err) {
      log.error({
        message: "Error while validating user session",
        session: request.session,
        error: err,
      });
      jsonErrorOut(response, 500);
      next(new Error());
      return;
    }

    if (result.length !== 1) {
      log.warn({
        message: "User had session with username/userid that does not exist.",
        result,
      });
      jsonErrorOut(response, 401);
      next(new Error());
      return;
    }

    next();
  });

  // error-catching middleware
  api.use((error, request, response, next) => {
    response.end();
  });

  // client-side batches up change operations and sends them all at once on a "save" action
  api.post("/v1/edit", async (request, response) => {
    const { body: query } = request || {};

    // "version" is the ms timestamp of the document that this edit is based on
    const missing = requireFields(["slice", "begin", "end"], query);

    if (missing) {
      jsonErrorOut(response, 400, "Missing field: " + missing);
      return;
    }

    try {
      const result = await storage.edit(request.session.userid, query);
      jsonSuccessOut(response, result);
    } catch (error) {
      const { publicMessage = "Internal server error.", code = 500 } = error;
      jsonErrorOut(response, code, publicMessage);
    }
  });

  api.get("/v1/read", async (request, response) => {
    const { query } = request || {};

    const missing = requireFields(["index"], query);

    if (missing) {
      jsonErrorOut(response, 400, "Missing field: " + missing);
      return;
    }

    try {
      const result = await storage.read(request.session.userid, query);
      jsonSuccessOut(response, result);
    } catch (error) {
      const { publicMessage = "Internal server error.", code } = error;
      jsonErrorOut(response, code, publicMessage);
    }
  });

  api.get("/v1/meta", async (request, response) => {
    try {
      const result = await storage.meta(request.session.userid);
      jsonSuccessOut(response, result);
    } catch (error) {
      const { publicMessage = "Internal server error.", code } = error;
      jsonErrorOut(response, code, publicMessage);
    }
  });

  api.post("/v1/publish", async (request, response) => {});

  // 404 handler
  api.use((request, response, next) => {
    jsonErrorOut(response, 404);
  });

  return api;
};
