const {
  ticket_expiry_sec: TICKET_EXPIRY_SEC,
  env: ENV,
} = require("../../../secrets.json");
const call = require("../lib/call");
const { jsonSuccessOut, jsonErrorOut } = require("../lib/jsonOut");

const express = require("express");
const auth = express.Router();

const bcrypt = require("bcrypt");
const cryptoRandomString = require("crypto-random-string");

module.exports = ({ knex, log, tickets }) => {
  auth.post("/claim", async (request, response) => {
    if (ENV === "PRO") {
      jsonErrorOut(response, 400, "Registration is disabled.");
      return;
    }

    if (!request.body.username || !request.body.password) {
      jsonErrorOut(response, 400, "Expected username and password.");
      return;
    }

    if (request.body.username.length > 20) {
      jsonErrorOut(response, 400, "Username is too long.");
      return;
    }

    // TODO check for all illegal characters

    if (typeof request.body.password !== "string" || !request.body.password) {
      jsonErrorOut(response, 400, "Invalid password.");
      return;
    }

    let [result, err] = await call(
      knex("users").insert({
        username: request.body.username,
        passhash: await bcrypt.hash(request.body.password, 10),
      }),
    );

    if (err) {
      switch (err.code) {
        case "ER_DUP_ENTRY":
          jsonErrorOut(response, 400, "Username is not available.");
          return;
        default:
          log.error({
            message: "Database error during user creation.",
            error: err,
          });
          jsonErrorOut(
            response,
            500,
            "Error creating user - please try again later.",
          );
          break;
      }
      return;
    }

    request.session.username = request.body.username;
    request.session.userid = result[0];

    jsonSuccessOut(response);
  });

  auth.post("/login", async (request, response) => {
    if (!request.body.username || !request.body.password) {
      jsonErrorOut(response, 400, "Expected both username and password.");
      return;
    }

    let [dbResult, err] = await call(
      knex
        .select("userid", "username", "passhash")
        .where({
          username: request.body.username,
        })
        .from("users"),
    );

    if (err) {
      log.error({
        message: "Database query for user and passhash failed",
        error: err,
      });
      jsonErrorOut(response, 500);
      return;
    }

    if (dbResult.length !== 1) {
      // TODO record failed login attempts
      jsonErrorOut(response, 401, "Incorrect username or password.");
      return;
    }

    const match = await bcrypt.compare(
      request.body.password,
      dbResult[0].passhash.toString("utf8"),
    );

    if (!match) {
      jsonErrorOut(response, 401, "Incorrect username or password.");
      return;
    }

    const { userid } = dbResult[0] || {};

    // good from here, log user in
    // authed sessions can be expected to include these keys:
    request.session.username = request.body.username;
    request.session.userid = userid;

    jsonSuccessOut(response, {
      authed: true,
      username: request.body.username,
    });
  });

  auth.post("/logout", async (request, response) => {
    request.session.destroy();
    jsonSuccessOut(response);
  });

  // returns authentication state and gives the username of the logged-in user if authed
  auth.get("/touch", async (request, response) => {
    if (request.session.username) {
      // ensure username is valid and has not expired; if invalid, clear it
      const [result, err] = await call(
        knex("users")
          .select("username", "userid")
          .where({
            username: request.session.username,
          }),
      );

      if (err) {
        jsonErrorOut(response, 500);
        return;
      }

      if (result.length !== 1) {
        request.session.destroy();
        request.session.username = undefined;
      }
    }

    if (request.session.username) {
      jsonSuccessOut(response, {
        authed: true,
        username: request.session.username,
      });
    } else {
      jsonSuccessOut(response, {
        authed: false,
      });
    }
  });

  // get ticket for websocket
  auth.get("/ticket", async (request, response) => {
    if (
      !request.session.username ||
      typeof request.session.userid !== "number"
    ) {
      jsonErrorOut(response, 401);
      return;
    }

    let [result, err] = await call(
      knex("users")
        .select("userid")
        .where({
          userid: request.session.userid,
          username: request.session.username,
        }),
    );

    if (err) {
      log.error({
        message: "Error while validating user session",
        session: request.session,
        error: err,
      });
      request.session.destroy();
      jsonErrorOut(response, 401);
      return;
    }

    let [ticketId, _] =
      Object.entries(tickets).find(
        ([key, { username, userid }]) =>
          username === request.session.username &&
          userid === request.session.userid,
      ) || [];

    if (!ticketId) {
      ticketId = cryptoRandomString({ length: 16, type: "url-safe" });
      tickets[ticketId] = {
        username: request.session.username,
        userid: request.session.userid,
        expires: Math.floor(Date.now() / 1000) + TICKET_EXPIRY_SEC,
      };
      log.debug({
        message: "No existing ticket found, creating new.",
        ticketId,
        ticket: tickets[ticketId],
      });
    } else {
      log.debug({
        message: "Reusing existing ticket",
        ticketId,
        ticket: tickets[ticketId],
      });
    }

    jsonSuccessOut(response, { ticketId });
  });

  auth.use((request, response, next) => {
    jsonErrorOut(response, 404);
  });

  return auth;
};
