module.exports = log => {
  class ApiError extends Error {
    constructor({
      publicMessage = "Internal server error.",
      debugMessage = publicMessage,
      code = publicMessage === debugMessage ? 400 : 500,
      ...meta
    } = {}) {
      super();
      this.name = "ServerError";
      this.message = debugMessage;
      this.publicMessage = publicMessage;
      this.code = code;
      this.meta = meta;

      log[code >= 500 ? "error" : "warn"]({
        message: debugMessage,
        ...this.meta,
      });
    }
  }

  return { ApiError };
};
