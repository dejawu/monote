// Storage engine backed by ProseMirror state

const { EditorState } = require("prosemirror-state");
const { Node, Slice, Fragment } = require("prosemirror-model");
const schema = require("../common/schema");

const shortid = require("shortid");

const fs = require("fs");
const path = require("path");

const ReadWriteLock = require("rwlock");

const STORE_PATH = path.join(
  process.cwd(),
  require("../../secrets").store_path,
);
const KEEPALIVE = require("../../secrets").storage_mem_keepalive_min;
const DEFAULT_CONTENT = require("../../secrets").default_note;

module.exports = log => {
  const { ApiError } = require("./errors")(log);

  // TODO adapt this to use the common/Storage interface
  class Storage {
    constructor() {
      this._instances = []; // holds all active storage instances

      process.on("SIGINT", () => {
        log.info("Persisting open storage instances before shutdown.");
        Promise.all(
          this._instances.map(instance => this.unload(instance.userid)),
        ).then(() => {
          log.info("All instances saved.");
          process.exit(0);
        });
      });
    }

    // # Main user operations
    // Will automatically keep-alive

    read(userid, params) {
      this.touch(userid);
      return this._instances[userid].read(params);
    }

    edit(userid, params) {
      this.touch(userid);
      return this._instances[userid].edit(params);
    }

    meta(userid) {
      this.touch(userid);
      return this._instances[userid].meta();
    }

    isActive(userid) {
      return !!this._instances[userid];
    }

    // # Other operations

    // keepalive instance if loaded; load if not
    touch(userid) {
      if (!this.isActive(userid)) {
        // TODO get latest change timestamp from db or user localstorage
        this._instances[userid] = new StorageInstance(userid, 0);
        log.debug({
          message: "User storage was not active, starting now",
          userid,
        });
      }

      clearTimeout(this._instances[userid].timeoutId);
      this._instances[userid].timeoutId = setTimeout(
        () => this.unload(userid),
        KEEPALIVE * 60 * 1000,
      );
    }

    async unload(userid) {
      log.debug({
        message: "Unloading user.",
        userid,
      });
      await this._instances[userid].persist();
      delete this._instances[userid];
    }
  }

  // TODO separate state management from storage engine, for easier testing
  class StorageInstance {
    constructor(userid) {
      this.version = 0; // millisecond timestamp of latest change
      this.path = path.join(STORE_PATH, userid.toString() + ".json");
      this.userid = userid;
      this.timeoutId = -1;

      // Initialize EditorState using stored JSON, or default document if no file
      let docJSON;
      if (fs.existsSync(this.path)) {
        try {
          const { content, version } = JSON.parse(
            fs.readFileSync(this.path).toString(),
          );

          docJSON = {
            type: "doc",
            content,
          };

          this.version = version || Date.now(); // gracefully set version if document is missing it
        } catch (err) {
          log.error({
            message: "Failed to read user document file.",
            error: err,
          });
          return;
        }
      } else {
        log.debug({
          message:
            "User did not have a document, creating one with default content",
          userid: this.userid,
        });
        docJSON = {
          type: "doc",
          content: DEFAULT_CONTENT,
        };
        this.version = Date.now();
      }

      this.stateLock = new ReadWriteLock();

      this.editorState = EditorState.create({
        schema,
        doc: Node.fromJSON(schema, docJSON),
      });

      // TODO find a way to not have to run this every time, e.g. cache
      this.updateToC();
    }

    // TODO remove altogether
    // TODO readlock
    // TODO cache results and only recompute if they're different
    // returns flat array of heading nodes
    // also validates heading ID's, throwing an error if invalid
    updateToC() {
      const toc = [];
      const ids = [];
      this.editorState.doc.forEach(
        (
          {
            type: { name: typeName } = {},
            attrs: { level, id } = {},
            textContent,
          } = {},
          offset,
          nodeGlobalIndex,
        ) => {
          if (typeName !== "heading") {
            return;
          }

          if (!id) {
            throw new ApiError({
              publicMessage: "Heading is missing id.",
              typeName,
              level,
              id,
              content: textContent,
            });
          }

          if (ids.includes(id)) {
            throw new ApiError({
              publicMessage: "Heading contains duplicate id.",
              typeName,
              level,
              id,
              content: textContent,
            });
          }

          ids.push(id);

          toc.push({
            level,
            content: textContent,
            globalIndex: nodeGlobalIndex,
            id,
          });
        },
      );

      this.toc = toc;
    }

    read({ index: globalIndex = 0, count = 1, toc = false, version = false }) {
      return new Promise((resolve, reject) => {
        this.stateLock.readLock(release => {
          // version param is optional but can be used to verify a state is up to date
          if (version && version !== this.version) {
            release();
            return reject(
              new ApiError({
                publicMessage: "Incorrect version.",
              }),
            );
          }

          const {
            doc: { childCount },
            doc,
          } = this.editorState;

          // JavaScript is the new PHP
          try {
            [globalIndex, count] = [globalIndex, count].map(val =>
              parseInt(val),
            );
          } catch (error) {
            release();
            return reject(
              new ApiError({
                publicMessage: "Invalid position, before, or end.",
                position: globalIndex,
                count,
                error,
              }),
            );
          }

          // for underflow, show the part of the requested range that does exist
          if (globalIndex < 0) {
            count = Math.max(0, count - (0 - globalIndex));
            globalIndex = 0;
          }

          const nodes = [];
          let i = globalIndex;
          let c = 0;
          while (c < count && i + c < childCount) {
            nodes.push(doc.content.child(i + c));
            c += 1;
          }

          if (toc) {
            this.updateToC();
          }

          resolve({
            slice: new Slice(Fragment.fromArray(nodes)).toJSON(),
            nodeCount: childCount,
            // delivered begin/end may not necessarily match requested position/count
            begin: !!nodes.length ? globalIndex : undefined,
            end: !!nodes.length ? i + c - 1 : undefined,
            toc: toc ? this.toc : undefined,
            version: this.version,
          });
          release();
        });
      });
    }

    // TODO real error handling
    // begin and end are node indexes, not character indexes
    edit({ slice, begin, end, version, toc = false }) {
      slice = Slice.fromJSON(schema, slice);

      try {
        begin = parseInt(begin);
        end = parseInt(end);
        version = parseInt(version);
      } catch (error) {
        throw new ApiError({
          publicMessage: "Invalid beginning or end position.",
          code: 400,
          end,
          error,
        });
      }

      if (version !== this.version) {
        throw new ApiError({
          publicMessage: "Invalid base version.",
          expected: this.version,
          got: version,
        });
      }

      // no negative numbers allowed
      if (begin < 0 || end < 0) {
        throw new ApiError({
          publicMessage: "Negative indexes are not allowed.",
        });
      }

      // end must be after begin
      if (begin > end) {
        throw new ApiError({
          publicMessage: "End must be after beginning.",
        });
      }

      // no empty documents
      if (
        !slice.content ||
        (slice.content.length === 1 && !slice.content[0].content)
      ) {
        throw new ApiError({
          publicMessage: "Cannot save an empty document.",
        });
      }

      return new Promise((resolve, reject) => {
        this.stateLock.writeLock(release => {
          // end must be before end of document
          if (end >= this.editorState.doc.childCount) {
            release();
            return reject(
              new ApiError({
                publicMessage: "End position was beyond end of document.",
              }),
            );
          }

          // number of nodes originally in the segment we're about to replace, NOT total number of nodes in document
          const originalNodeCount = end - begin + 1;
          const newNodeCount = slice.content.childCount;

          const toc = {};

          // find character positions that correspond to the range being replaced
          let beginCharPos = -1;
          let endCharPos = -1;
          // TODO cache offsets?
          this.editorState.doc.forEach((node, offset, nodeGlobalIndex) => {
            if (nodeGlobalIndex === begin) {
              beginCharPos = offset;
            }
            if (nodeGlobalIndex === end) {
              endCharPos = offset + node.nodeSize;
            }
            return false;
          });

          if (beginCharPos === -1 || endCharPos === -1) {
            release();
            return reject(
              new ApiError({
                code: 500,
                debugMessage:
                  "Could not find character position(s) for replacement region.",
                beginCharPos,
                endCharPos,
              }),
            );
          }

          try {
            const tr = this.editorState.tr;
            tr.replace(beginCharPos, endCharPos, slice);
            this.editorState = this.editorState.apply(tr);
          } catch (error) {
            release();
            return reject(
              new ApiError({
                debugMessage: "Error occurred while inserting new slice.",
                error,
              }),
            );
          }

          const headingsTr = this.editorState.tr;

          // validate ToC on new editor state
          const seenIds = [];
          this.editorState.doc.forEach(
            (
              {
                type: { name: typeName } = {},
                attrs: { level, id } = {},
                content,
              } = {},
              offset,
              nodeGlobalIndex,
            ) => {
              if (typeName !== "heading") {
                return;
              }

              if (!id || seenIds.includes(id)) {
                log.warn({
                  message: "Node was missing id or had duplicate id.",
                  content: content.toString(),
                  id,
                  seenIds,
                });

                headingsTr.setNodeMarkup(offset, null, {
                  level,
                  id: shortid.generate(),
                });
              } else {
                seenIds.push(id);
              }
            },
          );

          try {
            this.editorState = this.editorState.apply(headingsTr);
          } catch (error) {
            release();
            return reject(
              new ApiError({
                debugMessage: "Failed to update headings.",
                error,
              }),
            );
          }

          if (toc) {
            // TODO generate ToC from transaction rather than freshly recreating it
            try {
              this.updateToC();
            } catch (error) {
              release();
              return reject(
                new ApiError({
                  publicMessage: "Invalid heading.",
                  debugMessage:
                    "Error while generating ToC, likely invalid heading",
                  error,
                }),
              );
            }
          }

          this.version = Date.now();

          resolve({
            success: true,
            nodeCount: this.editorState.doc.childCount,
            newEnd: end + (newNodeCount - originalNodeCount),
            toc: toc ? this.toc : undefined,
            version: this.version,
          });
          release();
        });
      });
    }

    meta() {
      this.updateToC();
      return {
        version: this.version,
        toc: this.toc,
        // TODO also send app version; change key name to differentiate
      };
    }

    // write document to memory as JSON
    persist() {
      return new Promise((resolve, reject) => {
        log.debug("Persist operation waiting for lock, user: " + this.userid);
        this.stateLock.readLock(release => {
          log.debug("Persist operation acquired lock for user: " + this.userid);

          const contentString = JSON.stringify({
            version: this.version,
            content: this.editorState.doc.content,
          });

          fs.writeFile(this.path, contentString, "utf8", err => {
            if (err) {
              log.error({
                message: "Error persisting document file to memory.",
                error: err,
              });
              release();
              reject(err);
              return;
            }
            log.debug("Persist operation complete for user: " + this.userid);
            release();
            resolve();
          });
        });
      });
    }
  }

  return Storage;
};
