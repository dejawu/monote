const call = require("./lib/call");

module.exports = (io, { storage, knex, log, tickets }) => {
  io.use(async (socket, next) => {
    const { handshake } = socket;
    const { query: { ticketId } = {}, address } = handshake || {};
    if (!ticketId) {
      // this is (unfortunately) expected to happen under normal operation during reconnects, so it's a debug not a warn.
      log.debug({
        message: "Attempted socket handshake with missing ticket.",
        handshake: socket.handshake,
      });
      return next(new Error("Authentication error."));
    }

    const { username, userid, expires } = tickets[ticketId] || {};
    if (!username || !userid) {
      log.warn({
        message: "Attempted socket handshake with invalid ticket.",
        ticketId,
        address,
        tickets,
      });

      return next(new Error("auth"));
    }

    if (expires < Math.floor(Date.now() / 1000)) {
      log.warn({
        message: "Attempted socket handshake with expired ticket.",
        username,
        userid,
        expires,
        ticketId,
        address,
      });
      delete tickets[ticketId];
      return next(new Error("auth"));
    }

    // check that user is valid
    let [result, err] = await call(
      knex("users")
        .select("userid")
        .where({
          userid,
          username,
        }),
    );

    if (err) {
      log.error({
        message: "Error while validating socket user",
        handshake,
        error: err,
      });
      return next(new Error("internal"));
    }

    if (result.length !== 1) {
      log.warn({
        message:
          "User authenticated socket with username/userid that do not exist.",
        result,
        handshake,
      });
      return next(new Error("auth"));
    }

    log.debug({
      message: `User ${username} (id ${userid}) authenticated socket with ticket ${ticketId}.`,
    });

    delete tickets[ticketId];
    socket.handshake.query.username = username;
    socket.handshake.query.userid = userid;

    return next();
  });

  io.on("connection", socket => {
    const { handshake } = socket;
    const {
      query: { username, userid },
      address,
    } = handshake;

    socket.heartbeatTimeout = 2000;

    log.debug({
      message: `Connected to: ${username} (id ${userid}) at ${address}`,
    });

    socket.on("disconnect", async reason => {
      log.debug({
        message: `Disconnected from: ${username} (id ${userid}) at ${address}`,
        address: address,
      });
    });
  });
};
