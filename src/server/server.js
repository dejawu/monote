// local imports
const requireFields = require("./lib/requireFields");

// app config
const SECRETS = require("../../secrets.json");
const missingField = requireFields(
  [
    "env", // Environment setting: DEV, or PRO (use "PRO" for staging environment)
    "domain", // root domain of app
    "port", // port on which to run HTTP server
    "mysql_host",
    "mysql_user",
    "mysql_pass",
    "mysql_db",
    "store_path", // relative path for directory where user documents are stored
    "migration_user", // migration user in MySQL, needs EVENT and all permissions on ALL
    "migration_pass",
    "chunk_size", // number of nodes to load in per request. when unloading, the floor of half this value will be used (to prevent feedback loops.)
    "storage_mem_keepalive_min", // minimum minutes to keep a user's storage in memory after last action
    "ticket_expiry_sec", // number of seconds after creation until a WebSocket ticket expires
    "default_note", // Array of JSON-serialized ProseMirror Nodes
  ],
  SECRETS,
);
if (missingField) {
  console.log("Missing required config field: " + missingField);
  process.exit(0);
}

const cryptoRandomString = require("crypto-random-string");

// Express and related
const express = require("express");
const app = express();
const server = require("http").Server(app);
const path = require("path");
const bodyparser = require("body-parser");
const io = require("socket.io")(server, {
  transports: ["websocket", "polling"],
});

// database and related
const session = require("express-session");
const mysqlSessionStore = require("express-mysql-session")(session);
const sessionStore = new mysqlSessionStore({
  host: SECRETS.mysql_host,
  user: SECRETS.mysql_user,
  password: SECRETS.mysql_pass,
  database: SECRETS.mysql_db,
});
const knex = require("knex")({
  client: "mysql2",
  connection: {
    host: SECRETS.mysql_host,
    user: SECRETS.mysql_user,
    password: SECRETS.mysql_pass,
    database: SECRETS.mysql_db,
  },
});

// logging and instrumentation
const util = require("util");
const { createLogger, format, transports } = require("winston");
const log = createLogger({
  level: SECRETS.env === "DEV" ? "debug" : "info",
  format: format.combine(
    format.timestamp(),
    format.colorize(),
    format.errors({ stack: true }),
    format.printf(info =>
      `${info.timestamp} ${info.level}: ${info.message}\n${Object.entries(info)
        .filter(
          ([key]) =>
            key !== "level" && key !== "message" && key !== "timestamp",
        )
        .reduce(
          (acc, [key, val]) =>
            `${acc}${key}: ${util.inspect(val, { colors: true })}\n`,
          "",
        )}\n`.slice(0, -1),
    ),
  ),
  transports: [new transports.Console()],
});

// server-side editor state and storage
const Storage = require("./Storage")(log);
const tickets = []; // TODO automatic cleanup of expired tickets
const storage = new Storage();

// Socket.io bindings
require("./socket")(io, { storage, knex, log, tickets });

// express bindings
app.use(bodyparser.json());
app.use(
  session({
    store: sessionStore,
    resave: false,
    saveUninitialized: false,
    secret:
      SECRETS.env === "DEV"
        ? "dev" // key reused in dev mode for easier debugging
        : cryptoRandomString({ length: 256 }),
  }),
);

// TODO brute-force rate limiting middleware
app.use(bodyparser.json({ type: "application/json" }));

app.use("/", express.static("dist"));

app.use("/auth", require("./routes/auth")({ storage, knex, log, tickets }));
app.use("/api", require("./routes/api")({ storage, knex, log }));

// TODO 404 html page
app.use((request, response, next) => {
  response.status(404).end();
});

server.listen(SECRETS.port, () => {
  log.info({
    message: "HTTP server listening.",
  });
});
