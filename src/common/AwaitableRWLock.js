// wrapper for rwlock that returns promises

const ReadWriteLock = require("rwlock");

class AwaitableRWLock {
  constructor() {
    this.lock = new ReadWriteLock();
  }

  // read lock
  readLock() {
    return new Promise((resolve, reject) => {
      this.lock.readLock(release => resolve(release));
    });
  }

  // write lock
  writeLock() {
    return new Promise((resolve, reject) => {
      this.lock.writeLock(release => resolve(release));
    });
  }
}
