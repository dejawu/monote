/*
Interface defining common IO operations on document.

To implement, extend the Entry class and implement its methods.
*/
import { Node, Fragment, Slice } from "prosemirror-model";

import shortid from "shortid";

import schema from "./schema";

import { default_note as DEFAULT_NOTE } from "../../secrets";
import { replace } from "prosemirror-model/src/replace";

class Storage {
  constructor(entryClass) {
    this.Entry = entryClass;
    this.toc = [];

    // TODO init ToC
  }

  // not usually needed; edit will build the ToC as it runs
  buildToC() {
    const newToC = [];
    let nodeCount = 0;
    let entry = this.Entry.head;

    if (!entry || !entry.node) {
      // no entries means no storage, means no ToC
      this.toc = [];
      return;
    }

    while (entry && entry.node) {
      if (entry.node.type === "heading") {
        const node = entry.node;
        const headingNode = Node.fromJSON(schema, node);

        newToC.push({
          level: node.attrs.level,
          content: headingNode.textContent,
          globalIndex: nodeCount,
          id: node.attrs.id,
        });
      }

      entry = entry.next;
      nodeCount += 1;
    }

    this.toc = newToC;
  }

  // must run to completion before any transaction is run!
  async init() {
    if (!this.Entry.version) {
      let entry = this.Entry.head;

      DEFAULT_NOTE.forEach(node => {
        entry.node = node;
        entry.next = new this.Entry();
        entry = entry.next;
      });

      this.Entry.version = Date.now();
    }

    return this.buildToC();
  }

  // TODO remove ToC parameter and always send it
  async read({ index, count = 1 }) {
    // TODO clean up duplicate code once working
    let entry = this.Entry.head;
    let i = 0;
    // for underflow, show the part of the requested range that does exist
    if (index < 0) {
      count = Math.max(index, count - (0 - index));
      index = 0;
    }

    // monitor for duplicate entries
    const seenEntries = [];

    // seek up to index
    while (i < index) {
      if (!entry) {
        // index was beyond end of document
        return {
          slice: null,
          nodeCount: i,
        };
      }

      if (seenEntries.includes(entry.id)) {
        return {
          success: false,
          message: "Duplicate entry id: " + entry.id,
        };
      }

      seenEntries.push(entry.id);

      entry = entry.next;
      i += 1;
    }

    // now i === index

    // collect nodes until out of range to be read
    const nodes = [];
    let c = 0;
    while (c < count) {
      if (!entry || !entry.node) {
        break;
      }
      nodes.push(Node.fromJSON(schema, entry.node));

      if (!entry.id || seenEntries.includes(entry.id)) {
        return {
          success: false,
          message: "Invalid entry id: " + entry.id,
        };
      }

      seenEntries.push(entry.id);
      entry = entry.next;
      c += 1;
    }

    // TODO cache nodeCount and make this unnecessary?
    i += c;

    // continue counting until the end
    while (!!entry) {
      if (!entry || !entry.node) {
        break;
      }

      if (!entry.id || seenEntries.includes(entry.id)) {
        return {
          success: false,
          message: "Invalid entry id: " + entry.id,
        };
      }

      seenEntries.push(entry.id);
      entry = entry.next;
      i += 1;
    }

    return {
      slice: new Slice(Fragment.fromArray(nodes)).toJSON(),
      nodeCount: i,
      begin: !!nodes.length ? index : undefined,
      end: !!nodes.length ? index + c - 1 : undefined,
      toc: this.toc,
    };
  }

  async edit({ slice, begin, end }) {
    // no negative numbers allowed
    if (begin < 0 || end < 0) {
      return {
        success: false,
        message: `No negative numbers allowed. begin: ${end}, end: ${end}`,
      };
    }

    // end must be after begin
    if (begin > end) {
      return {
        success: false,
        message: `End must be after begin. begin: ${begin}, end: ${end}`,
      };
    }

    // no empty documents allowed
    if (
      !slice.content ||
      (slice.content.length === 1 && !slice.content[0].content)
    ) {
      return {
        success: false,
        message: `Cannot save an empty document.`,
      };
    }

    let entry = this.Entry.head;
    let i = 0;
    let nodeCount = 0;

    const originalNodeCount = end - begin + 1;
    // using length instead of childCount so we don't have to create a real Slice from JSON
    const newNodeCount = slice.content.length;

    let lastNodeInHead;
    let firstNodeInTail;

    const seenIds = [];
    const newToC = [];

    const processHeading = (entry, globalIndex = nodeCount) => {
      const node = entry.node;

      if (node.type !== "heading") {
        return;
      }

      if (!node.attrs.id || seenIds.includes(node.attrs.id)) {
        console.log(
          "Warning: heading has missing or duplicate id " + node.attrs.id,
        );
        node.attrs.id = shortid.generate();
        entry.node = node;
      } else {
        seenIds.push(node.attrs.id);
      }

      // TODO find a way to not need this?
      const headingNode = Node.fromJSON(schema, node);

      newToC.push({
        level: node.attrs.level,
        content: headingNode.textContent,
        globalIndex,
        id: node.attrs.id,
      });
    };

    // save the headings in the tail for processing later, so they are added in the right order
    const tailHeadings = [];

    // we can't know the true position of tail entries yet, only where they exist in the tail
    let positionInTail = 0;

    // count and process existing nodes
    do {
      if (i >= begin && i <= end) {
        // inside discarded range; queue for deletion
        entry.delete();
      } else if (i < begin) {
        // document head; can process right away
        processHeading(entry, nodeCount);
        nodeCount += 1;
      } else if (i > end) {
        // save heading for later processing
        if (entry.node && entry.node.type === "heading") {
          tailHeadings.push([entry, positionInTail]);
        }

        positionInTail += 1;
        nodeCount += 1;
      }

      // preserve nodes that new section will be connected to
      if (i === begin - 1) {
        lastNodeInHead = entry;
      } else if (i === end + 1) {
        firstNodeInTail = entry;
      }

      // advance entry
      entry = entry.next;

      i += 1;
    } while (entry && entry.hasNext());

    // save current tail in case it needs to be deleted
    const tailEntry = entry;

    // attach new segment to last node in head
    entry = lastNodeInHead;

    let replaceHead = begin === 0;

    slice.content.forEach((node, index) => {
      if (replaceHead) {
        // overwrite head with new node
        entry = this.Entry.head;
        replaceHead = false;
      } else {
        // attach node to previous entry
        entry.next = new this.Entry();
        entry = entry.next;
      }

      nodeCount += 1;

      entry.node = node;
      processHeading(entry, begin + index);
    });

    if (firstNodeInTail) {
      // attach last inserted entry to tail
      entry.next = firstNodeInTail;
    } else {
      // if there's no firstNodeInTail, that means our edit region ends where the document ends
      // the new tail will have been generated during slice.content.forEach, so we need to remove the old tail
      localStorage.removeItem(tailEntry.id);
    }

    // lastly, add the headings in the tail to the ToC in order
    tailHeadings.forEach(([e, p]) => {
      processHeading(e, p + begin + slice.content.length);
    });

    this.toc = newToC;

    return {
      success: true,
      nodeCount: nodeCount - 1,
      newEnd: end + (newNodeCount - originalNodeCount),
      toc: this.toc,
    };
  }
}

export default Storage;
